const { User } = require("../models");

const bcrypt = require("bcrypt");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return User.destroy(id);
  },

  find(id) {
    return User.findByPk(id);
  },

  findAll() {
    return User.findAll();
  },

  getTotalUser() {
    return User.count();
  },

  // register
  async registerNewUser(createArgs) {
    console.log("ini repository");

    const user = await User.findOne({
      where: {
        email: createArgs.email,
      },
    });

    // check if user not exist
    if (user) {
      if (user.email === createArgs.email) {
        console.log("masukkk");
        throw new Error(`user with email : ${user.email} already taken`);
      }
    }

    console.log("iinnniiiii");

    return User.create(createArgs);
  },

  async login(userArgs) {
    console.log(userArgs);
    const hash = bcrypt.hashSync(userArgs.password, 10);
    const user = await User.findOne({
      where: {
        email: userArgs.email,
      },
    });
    if (user) {
      const validate = await bcrypt.compare(userArgs.password, user.password);
      if (validate) {
        return User.findOne({
          attributes: { hash },
          where: {
            email: userArgs.email,
          },
        });
      } else {
        throw new Error("Wrong password");
      }
    }
    throw new Error("User not found");
  },
};

//   login(userArgs) {
//     console.log(userArgs)
//     return User.findOne({
//       where: {
//         email: userArgs.email
//       }
//     })
//   }
// };

// module.exports = {
//   create
//   register
//   fafwfwfw
// }
